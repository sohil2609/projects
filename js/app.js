

app.currentPageRecord = getTransactionsPerPage();
makeTable(app.currentPageRecord,app.ArrayHeaders);
createPageNumber();

//Function to Dynamically Make Tables based on Transaction Array Passed to it
function makeTable(Transactions,Headers){
	var $table = document.getElementById("transaction-table");
	
	isTbodyAttached = $table.getElementsByTagName('tbody').length > 0 ? true : false;
    //If Tbody is attached to the DOM, remove it to inset new view.
    if(!isTbodyAttached)
    {
		$table = getTableHead($table,Headers);
	}
	else
	{
		$table.removeChild($table.getElementsByTagName('tbody')[0]);
	}

    var $tbody = document.createElement('tbody');

    //This for loop dynamically creates each row, thier td elements and content inside it according to Transactions passed to it.
    for(var transaction_num=0;transaction_num < Transactions.length;transaction_num++)
    {
    	$tr_tbody = document.createElement('tr');     //Create TR element
    	columnNumber = 0;                               //TO attach event for mobile view
    	
    	setTransactionStatus($tr_tbody,Transactions[transaction_num].status);   //According to status class of TR element gets set
    	
    	for(var prop in Transactions[transaction_num])
    	{
    		if(Transactions[transaction_num].hasOwnProperty(prop) && prop != "status")   //td elements are created according properties in object
    		{
    			$td_tbody = document.createElement('td');
    			$td_tbody.innerHTML = Transactions[transaction_num][prop]; //Set Inner HTML value of td.
    			$td_tbody.setAttribute('data-column',app.ArrayHeaders[columnNumber].columnName) //For Mobile View ,will be used in CSS.
    			$tr_tbody.appendChild($td_tbody);
    		}
    		if(prop == "pickUpDate" && app.sortBy == "pickUpDate" && transaction_num != 0)   //Check if sort by pickup date and if so
    		{																				//Attach class last-transaction for showing marker.			

    			if(Transactions[transaction_num][prop].substring(0,2) != Transactions[transaction_num - 1][prop].substring(0,2))
    			{
    				previous_class = $tr_body.getAttribute('class')
    				$tr_body.setAttribute("class",previous_class + " last-transaction");
    			}
    		}
    		columnNumber += 1;
    	}
    	$tr_body = addButtons($tr_tbody);   // Add buttons to the row
    	$tbody.appendChild($tr_tbody);      //Add row to tbody
    }

    $table.appendChild($tbody);}           //Add tbody to the table

//Function to get the Table Header 
function getTableHead(table,Headers){
	var $thead = document.createElement('thead');
    $tr = document.createElement('tr');
    $tr_search = document.createElement('tr');
    for(var header_num=0;header_num < Headers.length;header_num++)
    {
    	$th = document.createElement('th');
        $td_search = document.createElement('td');
        $div = document.getElementById('mobile-sort');
        $nav = document.getElementsByTagName('nav')[0];
        $a = document.createElement('a');
        $a.innerHTML = Headers[header_num].columnName;
    	$th.innerHTML = Headers[header_num].columnName;
    	$th.style.width = Headers[header_num].width;
    	if(Headers[header_num].column_id != "none")
    	{
    		$input_search = GetSearchElement(Headers[header_num].column_id);
    		$td_search.appendChild($input_element);
    		$td_search.setAttribute('data-column',Headers[header_num].columnName)
    		$th.setAttribute('onclick','sort("'+Headers[header_num].column_id + '","' + Headers[header_num].type + '");');
    		$a.setAttribute('onclick','sort("'+Headers[header_num].column_id + '","' + Headers[header_num].type + '");');
   		}
   		else
   		{
   			$td_search.innerHTML = "";
   		}
   		$tr_search.appendChild($td_search);
    	$tr.appendChild($th);

    	$nav.appendChild($a);
    	
    }

    $thead.appendChild($tr);
    $thead.appendChild($tr_search,$tr);
    table.appendChild($thead);

    return table;}

//Function to Add buttons on each of the transaction, its pasrt of make table just segregated to make code cleaner
function addButtons($tr_tbody){
	$td_button = document.createElement('td');
	$show_button = document.createElement('button');
	$show_button.setAttribute('type','button');
	$show_button.setAttribute('class','btn btn-show');
	$show_button.innerHTML = "Show";

	$td_button.appendChild($show_button);
    $tr_tbody.appendChild($td_button);

	$td_button = document.createElement('td');
	$show_button = document.createElement('button');
	$show_button.setAttribute('type','button');
	$show_button.setAttribute('class','btn btn-edit');
	$show_button.innerHTML = "Edit";
	$td_button.appendChild($show_button);
    $tr_tbody.appendChild($td_button);

	$td_button = document.createElement('td');
	$show_button = document.createElement('button');
	$show_button.setAttribute('type','button');
	$show_button.setAttribute('class','btn btn-delete');
	$show_button.innerHTML = "Delete";
	$td_button.appendChild($show_button);
    $tr_tbody.appendChild($td_button);

	return $tr_tbody}

//Function to sort the array on the bases of sort type(sort by int or date or string) and which column to sort.
function sort(argsSortBY,sortType){
	
    if (app.sortBy == argsSortBY) {
        sortAsc = !sortAsc;
    }
    else {
        sortAsc = true;
    }
    app.sortBy = argsSortBY;
    app.ArrayTransactions = getSortedArray(app.ArrayTransactions,sortType,sortAsc);
    app.ArrayFilteredTransactions = getSortedArray(app.ArrayFilteredTransactions,sortType,sortAsc);
    app.currentPageRecord = getTransactionsPerPage();
    createPageNumber();
  	makeTable(app.currentPageRecord,app.ArrayHeaders);}

//Compare two dates based on two dates and args-isRequireGreater. That is if we want to check for smaller set it to false, 
//bigger -set it to truw
function CompareDate(date_str_1,date_str_2,isRequireGreater){
	date_1 = date_str_1.split('/');
	date_2 = date_str_2.split('/');
	dateObj_1 = new Date(date_1[2],date_1[1],date_1[0]);
	dateObj_2 = new Date(date_2[2],date_2[1],date_2[0]);
	if(dateObj_1 > dateObj_2 && isRequireGreater)
	{
		return true;
	}
	else if(dateObj_1 < dateObj_2 && !isRequireGreater)
	{
		return true;
	}
	else
	{
		return false;
	}}

//Get Search Text Box dynamically with a event attached to it
function GetSearchElement(data_model){
	$input_element = document.createElement('input');

	$input_element.setAttribute('type','text');

	$input_element.setAttribute('data-model',data_model);

	$input_element.setAttribute('class','search-input');

	$input_element.oninput = filter;	}

//Call back function attached to seach text box, when value in the search text box changes.
function filter(){
	app.ArrayFilteredTransactions = [];    //Construct the FilterTransaction acc to Search object
	$current_search_element = this;        //Get the current text being altered

	app.SearchObject[this.getAttribute('data-model')] = this.value.trim();   //Add it or change in the Search Object
	
	if(app.SearchObject[this.getAttribute('data-model')].length <= 0)    //If it beacomes empty remove it from Search
	{                                                                   //  object so we can save time in iterating it hence better performance
		delete app.SearchObject[this.getAttribute('data-model')];
		$current_search_element.style.backgroundImage = 'url("img_svg/searchicon.png")';
	}
	else
	{
		$current_search_element.style.backgroundImage = "none";
	}
	for(var transaction_num = 0;transaction_num < app.ArrayTransactions.length;transaction_num++)
	{
		isFiltered = false;
		isFilteredByCost = false;
	    for(var key in app.SearchObject)
	    {
	    	if(key == "cost")   //If search is by cost then >,<,= can be used to search
	    	{
	    		type = app.SearchObject[key][0];
	    		search_value = parseInt(app.SearchObject[key].substring(1));
	    		isFilteredByCost = !isNaN(search_value) ? 
	    							getIntFilter(app.ArrayTransactions[transaction_num][key],search_value,type,$current_search_element) 
	    						  : false;
	    	}
	    	else                         //Normal search using string
	    	{
	    		var value = app.ArrayTransactions[transaction_num][key].toUpperCase();
	    		if((value.indexOf(app.SearchObject[key].toUpperCase()) <= -1))
	    		isFiltered = true;
	    	}
	    }
	    if(!isFiltered && !isFilteredByCost)   //If transaction is not filtered by cost as well as other parameters it will be push to filterArray
	    {
	    	app.ArrayFilteredTransactions.push(app.ArrayTransactions[transaction_num]);
	    }
	}
	transactionsPerPage = getTransactionsPerPage();
	createPageNumber();
	makeTable(transactionsPerPage,app.ArrayHeaders);}

//Integer Filter Based on >,< or = values.
function getIntFilter(value,search_value,type,$current_search_element){
	isFilteredByCost = false;
	switch(type)
	{
		case '>':
		if(search_value >= value)
			isFilteredByCost = true;
		break;
		case '<':
		if(search_value <= value)
			isFilteredByCost = true;
		break;
		case '=':
		if(search_value != value)
			isFilteredByCost = true;
	}
	return isFilteredByCost;}

//This function sort the array, it is a part of the of main function - sort()
function getSortedArray($arrayTransactions,sortType,sortAsc){
	var switching = true;
    while (switching) {
        switching = false;
        for (i = 0; i < ($arrayTransactions.length - 1) ; i++) {
        	
            if (sortAsc) {
            		isSwitch = sortType == "string" ? $arrayTransactions[i][app.sortBy].toUpperCase() > $arrayTransactions[i + 1][app.sortBy].toUpperCase() : false;
           		    isSwitch = sortType == "date" ?	 CompareDate($arrayTransactions[i][app.sortBy],$arrayTransactions[i+1][app.sortBy],true) : isSwitch;
          		    isSwitch = sortType == "int"  ?  $arrayTransactions[i][app.sortBy] > $arrayTransactions[i + 1][app.sortBy] : isSwitch;
                    if (isSwitch) 
                    {
                        var temp = $arrayTransactions[i];
                        $arrayTransactions[i] = $arrayTransactions[i + 1];
                        $arrayTransactions[i + 1] = temp;
                        switching = true;
                    }
                
            }
            else {
            	isSwitch = sortType == "string" ? $arrayTransactions[i][app.sortBy].toUpperCase() < $arrayTransactions[i + 1][app.sortBy].toUpperCase() : false;
            	isSwitch = sortType == "date" ?	 CompareDate($arrayTransactions[i][app.sortBy],$arrayTransactions[i+1][app.sortBy],false) : isSwitch;
            	isSwitch = sortType == "int"  ?  $arrayTransactions[i][app.sortBy] < $arrayTransactions[i + 1][app.sortBy] : isSwitch;
                if(isSwitch)
                 {
                    var temp = $arrayTransactions[i];
                    $arrayTransactions[i] = $arrayTransactions[i + 1];
                    $arrayTransactions[i + 1] = temp;
                    switching = true;
                }
            }
        }
    }
    return $arrayTransactions;}

//This function gives records per page according to the page number passed in as argument.
function getTransactionsPerPage(pageNumber){
	pageNumber = pageNumber || 1;
	ArrayTransactionsPerPage = [];
	for(var record = 1;record <= app.PAGE_PER_RECORD;record++)
	{
		var transaction_num = app.PAGE_PER_RECORD*(pageNumber-1) + record - 1;
		if(transaction_num < app.ArrayFilteredTransactions.length)
		ArrayTransactionsPerPage.push(app.ArrayFilteredTransactions[transaction_num]);
	}

	return ArrayTransactionsPerPage;}

//This function create page-number buttons and attach event to it using javascript closures.
function createPageNumber(){
	totalRecords = app.ArrayFilteredTransactions.length;
	totalPages = totalRecords%(app.PAGE_PER_RECORD) == 0 ? totalRecords/app.PAGE_PER_RECORD : Math.floor(totalRecords/app.PAGE_PER_RECORD)+ 1;
	$div = document.getElementById('Page_Buttons');
	removePageButtons($div);
	for(var page=1;page <= totalPages;page++)
	{
		var $page_button = document.createElement('button');
		$page_button.setAttribute('type','button');
		if(page == 1)
			$page_button.setAttribute('class','active');
		$page_button.innerHTML = page;
		//Immediately invoked function to preserve the value of pageNumber for the return function.
		$page_button.onclick = (function(pageNumber){
			return function(){
			$previous_page_button = document.getElementsByClassName('active')[0];
			if($previous_page_button != undefined)
				$previous_page_button.classList.remove("active");
			
			this.setAttribute('class','active');
			
			app.currentPageRecord = getTransactionsPerPage(pageNumber);
			
			makeTable(app.currentPageRecord,app.Headers);
		}
	})(page);
		
	$div.appendChild($page_button);
	}}

//This function is to remove the current page buttons on filter.
function removePageButtons(argsDiv){
	$pageButtons = argsDiv.getElementsByTagName('button');
	while($pageButtons.length != 0)
	{
		argsDiv.removeChild($pageButtons[0]);
	}}

//This function sets the transaction status and give class to the element dynamically, which on UI is shown using different types of trucks
function setTransactionStatus(element,status){
	if(status == "Active")
    	{
    		element.setAttribute("class","active-status");
    	}
    if(status == "Complete")
    	{
    		element.setAttribute("class","complete-status");
    	}
    if(status == "Cancelled")
    	{
    		element.setAttribute("class","cancel-status");
    	}}

//This function is used to close the SortBy navigation bar
function closeNav(){
	$element = document.getElementById('mobile-sort');
	$button = document.getElementsByClassName('btn-sort')[0];
	$element.setAttribute('class','close-nav');
	$button.style.display = "block";}
 
 //This function is used to open the SortBy navigation bar
function openNav(){
	$element = document.getElementById('mobile-sort');
	$button = document.getElementsByClassName('btn-sort')[0];
	$element.removeAttribute('class');
	$button.style.display = "none";}