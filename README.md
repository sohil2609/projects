### What is this repository for? ###

This repository is for displaying Transactions in paginated view with following requirements:

1. Filter Done - Filter in 'cost' column is like - **>12000** will give all records greater than 12000. **<12000** will give records less than 12000 and **=20000** is self explanatory.Others are normal filters.

2. Sort Column - On clicking on header. Table will be sorted in asc order, again clicking it will sort desc order.

3.Paginated View - 30 records per page. Can change by changing the constant.

4. Status is shown by trucks which also explains the purpose for what exactly website is used. I think placement of trucks is self explanatory which truck is for what else its my lack of creativity.

5. When sort by date there will be big border between last transaction and first transaction of next date.

6. Responsive in desktop,tablet and mobile. Cards like beautiful structured in Mobile View.

### How do I get set up? ###

Just clone and run index.html file. 
If you want to alter the Data Transactions, dummy database type of structure is formed in: data.js. Edit 2 Arrays for data modification.